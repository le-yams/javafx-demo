/*
 * FXMLDocumentController.java
 */
package demo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *
 * @author Yann D'Isanto
 */
public class HelloworldController implements Initializable {

    @FXML
    private void sayHello(ActionEvent event) {
        System.out.println("Hello World!");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

}
