/*
 * DemoCharts.java
 */
package demo;

import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * TODO:
 *   - put the bar chart tab related fxml in its own file (use fx:include, look at pie chart for example)
 *   - skin with css
 * 
 * @author Yann D'Isanto
 */
public class DemoCharts extends Application {
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(
                getClass().getResource("/demo/ui/DemoCharts.fxml"),
                ResourceBundle.getBundle(DemoCharts.class.getName()));
        
        Scene scene = new Scene(root, 800, 600);
        
        primaryStage.setTitle("Demo");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);    
    }
    
}
