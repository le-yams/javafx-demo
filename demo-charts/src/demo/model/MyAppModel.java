/*
 * MyAppModel.java
 */
package demo.model;

import javafx.beans.property.ReadOnlyListWrapper;
import javafx.collections.FXCollections;

/**
 *
 * @author Yann D'Isanto
 */
public interface MyAppModel {

    
    ReadOnlyListWrapper<User> USERS = new ReadOnlyListWrapper<>(FXCollections.observableArrayList());
    
    ReadOnlyListWrapper<Task> TASKS = new ReadOnlyListWrapper<>(FXCollections.observableArrayList());
    
    
    static User createUser(String name) {
        User user = new User(name);
        USERS.add(user);
        return user;
    }
    
    static Task createTask(String name) {
        return createTask(name, null);
    }
    
    static Task createTask(String name, User owner) {
        return createTask(name, owner, Task.State.TODO, 0, 0);
    }
    
    static Task createTask(String name, User owner, Task.State state, int estimatedHours, int effectiveHours) {
        Task task = new Task(name, owner, state, estimatedHours, effectiveHours);
        TASKS.add(task);
        return task;
    }
}
