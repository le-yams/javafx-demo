/*
 *  PieChartTabController.java
 */
package demo.ui;

import demo.bindings.UserTasksCount;
import demo.model.User;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;

/**
 * @author Yann D'Isanto
 */
public class PieChartTabController implements Initializable {

    @FXML
    PieChart pieChart;

    private final Map<User, PieChart.Data> usersPieData = new HashMap<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        pieChart.setData(FXCollections.observableArrayList());

    }    
    
    public void addUserPieData(User user) {
        PieChart.Data data = new PieChart.Data("", 0);
        data.nameProperty().bind(user.nameProperty());
        data.pieValueProperty().bind(new UserTasksCount(user));
        usersPieData.put(user, data);
        pieChart.getData().add(data);
    }

    public void removeUserPieData(User user) {
        PieChart.Data data = usersPieData.remove(user);
        data.nameProperty().unbind();
        data.pieValueProperty().unbind();
        pieChart.getData().remove(data);
    }

}
