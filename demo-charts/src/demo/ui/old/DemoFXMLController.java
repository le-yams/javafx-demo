/*
 * DemoFXMLController.java
 */
package demo.ui.old;

import demo.ui.components.UserNode;
import demo.ui.components.TaskNode;
import demo.bindings.DeepListBinding;
import demo.bindings.UserTasksCount;
import demo.bindings.UserTasksWithStateCount;
import demo.ui.components.TaskPane;
import demo.ui.components.UserPane;
import demo.model.MyAppModel;
import demo.model.Task;
import demo.model.User;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.Observable;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Yann D'Isanto
 */
public class DemoFXMLController implements Initializable {

    @FXML
    TreeView<String> treeView;

    @FXML
    TreeItem<String> usersNode;

    @FXML
    TreeItem<String> tasksNode;

    @FXML
    Button editUserButton;

    @FXML
    Button editTaskButton;

    @FXML
    PieChart pieChart;

    @FXML
    BarChart<String, Number> barChart;

    private final Map<User, UserNode> userNodes = new HashMap<>();

    private final Map<Task, TaskNode> taskNodes = new HashMap<>();

    private final Map<User, PieChart.Data> usersPieData = new HashMap<>();

    private final Map<Task.State, XYChart.Series<String, Number>> barSeries = new HashMap<>();

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TreeItem<String> treeRoot = new TreeItem<>("root");
        treeRoot.getChildren().addAll(usersNode, tasksNode);
        treeView.setRoot(treeRoot);
        treeView.setCellFactory((TreeView<String> param) -> {
            return new CustomCell();
        });
        MyAppModel.USERS.addListener(this::onUsersListChanged);
        MyAppModel.TASKS.addListener(this::onTasksListChanged);

        editUserButton.disableProperty().bind(new BooleanBinding() {

            {
                bind(treeView.getSelectionModel().selectedItemProperty());
            }

            @Override
            protected boolean computeValue() {
                TreeItem<String> node = treeView.getSelectionModel().getSelectedItem();
                return node instanceof UserNode;
            }
        }.not());
        editTaskButton.disableProperty().bind(new BooleanBinding() {

            {
                bind(treeView.getSelectionModel().selectedItemProperty());
            }

            @Override
            protected boolean computeValue() {
                TreeItem<String> node = treeView.getSelectionModel().getSelectedItem();
                return node instanceof TaskNode;
            }
        }.not());

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        pieChart.setData(pieChartData);

        CategoryAxis xAxis = (CategoryAxis) barChart.getXAxis();
        new DeepListBinding<User, ObservableList<String>>(MyAppModel.USERS) {

            @Override
            protected Observable[] elementObservables(User user) {
                return new Observable[]{user.nameProperty()};
            }

            @Override
            protected ObservableList<String> computeValue() {
                return MyAppModel.USERS.stream().map(user -> user.getName()).collect(Collectors.toCollection(FXCollections::observableArrayList));
            }
        }.addListener((ObservableValue<? extends ObservableList<String>> observable, ObservableList<String> oldValue, ObservableList<String> newValue) -> {
            xAxis.setCategories(newValue);
        });

        NumberAxis yAxis = (NumberAxis) barChart.getYAxis();
        yAxis.setTickUnit(1);
        
        barChart.setAnimated(false);
        for (Task.State state : Task.State.values()) {
            final XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(state.name());
            barSeries.put(state, series);
            barChart.getData().add(series);
        }
    }

    private void addUserPieData(User user) {
        PieChart.Data data = new PieChart.Data("", 0);
        data.nameProperty().bind(user.nameProperty());
        data.pieValueProperty().bind(new UserTasksCount(user));
        usersPieData.put(user, data);
        pieChart.getData().add(data);
    }

    private void addUserBarData(User user) {
        for (Task.State state : Task.State.values()) {
            XYChart.Series<String, Number> series = barSeries.get(state);
            XYChart.Data<String, Number> data = new XYChart.Data<>(user.getName(), 0);
            data.XValueProperty().bind(user.nameProperty());
            data.YValueProperty().bind(new UserTasksWithStateCount(user, state));
            series.getData().add(data);
        }
    }

    private void removeUserBarData(User user) {

        for (Task.State state : Task.State.values()) {
            XYChart.Series<String, Number> series = barSeries.get(state);
            XYChart.Data<String, Number> data = series.getData().stream()
                    .filter(d -> user.equals(d.getExtraValue()))
                    .findAny().get();
            series.getData().remove(data);
            data.XValueProperty().unbind();
            data.YValueProperty().unbind();
        }
    }
    
    private void removeUserPieData(User user) {
        PieChart.Data data = usersPieData.remove(user);
        data.nameProperty().unbind();
        data.pieValueProperty().unbind();
        pieChart.getData().remove(data);
    }

    private void onUsersListChanged(Change<? extends User> c) {
        while (c.next()) {
            if (c.wasPermutated() == false) {
                for (User user : c.getRemoved()) {
                    UserNode node = userNodes.remove(user);
                    usersNode.getChildren().remove(node);
                    removeUserPieData(user);
                    removeUserBarData(user);
                }
                for (User user : c.getAddedSubList()) {
                    UserNode node = new UserNode(user);
                    usersNode.getChildren().add(node);
                    userNodes.put(user, node);
                    treeView.getSelectionModel().select(node);
                    addUserPieData(user);
                    addUserBarData(user);
                }
            }
        }
    }

    private void onTasksListChanged(Change<? extends Task> c) {

        while (c.next()) {
            if (c.wasPermutated() == false) {
                for (Task task : c.getRemoved()) {
                    TaskNode node = taskNodes.remove(task);
                    tasksNode.getChildren().remove(node);
                }
                for (Task task : c.getAddedSubList()) {
                    TaskNode node = new TaskNode(task);
                    tasksNode.getChildren().add(node);
                    taskNodes.put(task, node);
                    treeView.getSelectionModel().select(node);
                }
            }
        }
    }
    
    @FXML
    void close(ActionEvent evt) {
        ((Stage) getWindow()).close();
    }

    @FXML
    void addUser(ActionEvent evt) {
        User user = UserPane.showCreateDialog(getWindow());
        usersNode.setExpanded(true);
    }

    @FXML
    void editUser(ActionEvent evt) {
        User user = ((UserNode) treeView.getSelectionModel().getSelectedItem()).getUser();
        UserPane.showEditDialog(getWindow(), user);
    }

    @FXML
    void addTask(ActionEvent evt) {
        Task task = TaskPane.showCreateDialog(getWindow());
        tasksNode.setExpanded(true);
    }

    @FXML
    void editTask(ActionEvent evt) {
        Task task = ((TaskNode) treeView.getSelectionModel().getSelectedItem()).getTask();
        TaskPane.showEditDialog(getWindow(), task);
    }

    private Window getWindow() {
        return treeView.getScene().getWindow();
    }

    class CustomCell extends TextFieldTreeCell<String> {

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            TreeItem<String> node = getTreeItem();
            if (node instanceof UserNode) {
                setContextMenu(createContextMenu(
                        (ActionEvent t) -> {
                            UserPane.showEditDialog(getWindow(), ((UserNode) node).getUser());
                        }, (ActionEvent t) -> {
                            MyAppModel.USERS.remove(((UserNode) node).getUser());
                        }));
            } else if (node instanceof TaskNode) {
                setContextMenu(createContextMenu(
                        (ActionEvent t) -> {
                            TaskPane.showEditDialog(getWindow(), ((TaskNode) node).getTask());
                        }, (ActionEvent t) -> {
                            MyAppModel.TASKS.remove(((TaskNode) node).getTask());
                        }));
            } else {
                setContextMenu(null);
            }
        }

        private ContextMenu createContextMenu(EventHandler<ActionEvent> editAction, EventHandler<ActionEvent> removeAction) {
            ContextMenu menu = new ContextMenu();
            MenuItem editMenuItem = new MenuItem("Edit");
            editMenuItem.setOnAction(editAction);
            MenuItem removeMenuItem = new MenuItem("Remove");
            removeMenuItem.setOnAction(removeAction);
            menu.getItems().addAll(editMenuItem, new SeparatorMenuItem(), removeMenuItem);
            return menu;
        }

    }

}
