/*
 * DemoChartsController.java
 */
package demo.ui;

import demo.bindings.DeepListBinding;
import demo.bindings.UserTasksWithStateCount;
import demo.model.MyAppModel;
import demo.model.Task;
import demo.model.User;
import demo.ui.components.DemoTreeView;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Yann D'Isanto
 */
public class DemoChartsController implements Initializable {

    @FXML
    BorderPane rootPane;

    @FXML
    DemoTreeView demoTreeView;

    @FXML
    PieChartTabController pieChartTabController;

    @FXML
    BarChart<String, Number> barChart;
    
    private final Map<Task.State, XYChart.Series<String, Number>> barSeries = new HashMap<>();

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MyAppModel.USERS.addListener(this::onUsersListChanged);

        CategoryAxis xAxis = (CategoryAxis) barChart.getXAxis();
        new DeepListBinding<User, ObservableList<String>>(MyAppModel.USERS) {

            @Override
            protected Observable[] elementObservables(User user) {
                return new Observable[]{user.nameProperty()};
            }

            @Override
            protected ObservableList<String> computeValue() {
                return MyAppModel.USERS.stream().map(user -> user.getName()).collect(Collectors.toCollection(FXCollections::observableArrayList));
            }
        }.addListener((ObservableValue<? extends ObservableList<String>> observable, ObservableList<String> oldValue, ObservableList<String> newValue) -> {
            xAxis.setCategories(newValue);
        });

        NumberAxis yAxis = (NumberAxis) barChart.getYAxis();
        yAxis.setTickUnit(1);

        barChart.setAnimated(false);
        for (Task.State state : Task.State.values()) {
            final XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(state.name());
            barSeries.put(state, series);
            barChart.getData().add(series);
        }
    }

    private void onUsersListChanged(Change<? extends User> c) {
        while (c.next()) {
            if (c.wasPermutated() == false) {
                for (User user : c.getRemoved()) {
                    pieChartTabController.removeUserPieData(user);
                    removeUserBarData(user);
                }
                for (User user : c.getAddedSubList()) {
                    pieChartTabController.addUserPieData(user);
                    addUserBarData(user);
                }
            }
        }
    }
    
    private void addUserBarData(User user) {
        for (Task.State state : Task.State.values()) {
            XYChart.Series<String, Number> series = barSeries.get(state);
            XYChart.Data<String, Number> data = new XYChart.Data<>(user.getName(), 0);
            data.XValueProperty().bind(user.nameProperty());
            data.YValueProperty().bind(new UserTasksWithStateCount(user, state));
            series.getData().add(data);
        }
    }

    private void removeUserBarData(User user) {
        for (Task.State state : Task.State.values()) {
            XYChart.Series<String, Number> series = barSeries.get(state);
            XYChart.Data<String, Number> data = series.getData().stream()
                    .filter(d -> user.equals(d.getExtraValue()))
                    .findAny().get();
            series.getData().remove(data);
            data.XValueProperty().unbind();
            data.YValueProperty().unbind();
        }
    }
    
    @FXML
    void exit(ActionEvent evt) {
        Platform.exit();
    }

}
