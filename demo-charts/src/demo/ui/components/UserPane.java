/*
 * UserPane.java
 */
package demo.ui.components;

import demo.model.MyAppModel;
import demo.model.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Yann D'Isanto
 */
public class UserPane extends GridPane implements Initializable {

    private final ObjectProperty<User> user = new SimpleObjectProperty<>();

    @FXML
    TextField nameField;

    @FXML
    CheckBox disabledField;

    public UserPane() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("UserPane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        disableProperty().bind(user.isNull());
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        user.addListener(this::userChanged);
    }

    private void userChanged(ObservableValue<? extends User> observable, User oldUser, User newUser) {
        if (oldUser != null) {
            nameField.textProperty().unbindBidirectional(oldUser.nameProperty());
            disabledField.selectedProperty().unbindBidirectional(oldUser.disabledProperty());
        }
        if (newUser != null) {
            nameField.textProperty().bindBidirectional(newUser.nameProperty());
            disabledField.selectedProperty().bindBidirectional(newUser.disabledProperty());
        }
    }

    public User getUser() {
        return user.get();
    }

    public void setUser(User value) {
        user.set(value);
    }

    public ObjectProperty<User> userProperty() {
        return user;
    }

    public static void showEditDialog(Window owner, User user) {
        UserEditDialog dialog = new UserEditDialog(owner, user);
        dialog.showAndWait();
    }

    public static User showCreateDialog(Window owner) {
        UserCreateDialog dialog = new UserCreateDialog(owner);
        dialog.showAndWait();
        if (dialog.ret == UserCreateDialog.OK) {
            return dialog.userPane.getUser();
        }
        return null;
    }

    static abstract class AbstractUserDialog extends Stage {

        static final int OK = 1;

        static final int CANCEL = 2;

        static final int CLOSE = 3;

        final UserPane userPane = new UserPane();

        final HBox buttons = new HBox(5);

        int ret = CLOSE;

        public AbstractUserDialog(Window owner) {
            initOwner(owner);
            initModality(Modality.APPLICATION_MODAL);
            buttons.setAlignment(Pos.CENTER_RIGHT);
            GridPane gridPane = new GridPane();
            gridPane.add(userPane, 0, 0, 2, 1);
            gridPane.add(buttons, 1, 1);
            GridPane.setVgrow(buttons, Priority.NEVER);
            GridPane.setHgrow(buttons, Priority.NEVER);
            GridPane.setMargin(buttons, new Insets(20, 5, 5, 5));
            Scene scene = new Scene(gridPane);
            setScene(scene);
        }

        void okAction(ActionEvent event) {
            MyAppModel.USERS.add(userPane.getUser());
            ret = OK;
            close();
        }

        void cancelAction(ActionEvent event) {
            ret = CANCEL;
            close();
        }

        void closeAction(ActionEvent event) {
            close();
        }
    }

    static class UserCreateDialog extends AbstractUserDialog {

        private static int id = 1;
                
        public UserCreateDialog(Window owner) {
            super(owner);
            Button okButton = new Button("Ok");
            Button cancelButton = new Button("Cancel");
            okButton.setPrefWidth(70);
            cancelButton.setPrefWidth(70);
            okButton.setOnAction(this::okAction);
            cancelButton.setOnAction(this::cancelAction);
            buttons.getChildren().addAll(okButton, cancelButton);
            userPane.setUser(new User("User " + nextId()));
            userPane.nameField.setOnAction(this::okAction);
        }
        
        private static int nextId() {
            return id ++;
        }
    }

    static class UserEditDialog extends AbstractUserDialog {

        public UserEditDialog(Window owner, User user) {
            super(owner);
            Button closeButton = new Button("Ok");
            closeButton.setOnAction(this::closeAction);
            buttons.getChildren().add(closeButton);
            userPane.setUser(user);
            userPane.nameField.setOnAction(this::closeAction);
        }

    }
}
