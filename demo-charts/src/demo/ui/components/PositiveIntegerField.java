
package demo.ui.components;

import java.util.regex.Pattern;
import javafx.scene.control.TextField;

/**
 *
 * @author Yann D'Isanto
 */
public class PositiveIntegerField extends TextField {

    private static final Pattern INT = Pattern.compile("\\d+");

    @Override
    public void replaceSelection(String replacement) {
        if (isIntOrEmpty(replacement)) {
            super.replaceSelection(replacement);
        }
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (isIntOrEmpty(text)) {
            super.replaceText(start, end, text);
        }
    }

    private boolean isIntOrEmpty(String text) {
        return text.isEmpty() || INT.matcher(text).matches();
    }
}
