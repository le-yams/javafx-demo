package demo.ui.components;

import demo.model.Task;
import javafx.beans.binding.StringBinding;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 *
 * @author Yann D'Isanto
 */
public class TaskNode extends TreeItem<String> {

    private final Task task;

    public TaskNode(Task task) {
        super(task.getName(), new ImageView("/demo/ui/components/resources/task.png"));
        this.task = task;
        valueProperty().bind(new StringBinding() {
            {
                bind(task.nameProperty(), task.ownerProperty());
            }

            @Override
            protected String computeValue() {
                StringBuilder sb = new StringBuilder().append(task.getName());
                if (task.getOwner() != null) {
                    sb.append(" - ").append(task.getOwner());
                }
                return sb.toString();
            }
        });
    }

    public Task getTask() {
        return task;
    }

}
