
package demo.bindings;

import demo.model.MyAppModel;
import demo.model.Task;
import demo.model.Task.State;
import javafx.beans.Observable;

/**
 *
 * @author Yann D'Isanto
 */
public class TasksWithStateCount extends DeepListBinding<Task, Double> {

    private final State state;
    
    public TasksWithStateCount(State state) {
        super(MyAppModel.TASKS);
        this.state = state;
    }

    
    @Override
    protected Observable[] elementObservables(Task task) {
        return new Observable[]{ task.stateProperty() };
    }

    @Override
    protected Double computeValue() {
        return (double) MyAppModel.TASKS.stream().filter(this::filterTask).count();
    }

    private boolean filterTask(Task task) {
        return task.getState() == state;
    }
}
