
package demo.bindings;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Yann D'Isanto
 */
public class NumberAsDoubleBinding extends DoubleBinding {

    private final ObservableValue<Number> number;

    public NumberAsDoubleBinding(ObservableValue<Number> number) {
        bind(number);
        this.number = number;
    }

    @Override
    protected double computeValue() {
        return number.getValue().doubleValue();
    }

    public static DoubleExpression asDouble(ObservableValue<Number> number) {
        return new NumberAsDoubleBinding(number);
    }
}
