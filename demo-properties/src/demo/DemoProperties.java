/*
 * MainProperties.java
 */
package demo;

import demo.model.MyAppModel;
import demo.model.User;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 *
 * TODO: 
 *   - dynamically display users list size
 *   - dynamically display user name change 
 * 
 * @author Yann D'Isanto
 */
public class DemoProperties extends Application implements Runnable {
    
    @Override
    public void run() {
        MyAppModel.createUser("user1");
        System.out.println("1 user added, new users count is " + MyAppModel.USERS.size());
        MyAppModel.createUser("user2");
        System.out.println("1 user added, new users count is " + MyAppModel.USERS.size());
        MyAppModel.createUser("user3");
        System.out.println("1 user added, new users count is " + MyAppModel.USERS.size());
        User user = MyAppModel.createUser("user4");
        System.out.println("1 user added, new users count is " + MyAppModel.USERS.size());
        user.setName("Luke");
        System.out.println("renamed 'user4' to 'Luke'");
        MyAppModel.USERS.remove(user);
        System.out.println("1 user removed, new users count is " + MyAppModel.USERS.size());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new DemoProperties().run();
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    }

}
