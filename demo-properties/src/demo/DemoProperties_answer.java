/*
 * Main1.java
 */
package demo;

import demo.model.MyAppModel;
import demo.model.User;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.stage.Stage;

/**
 *
 * @author Yann D'Isanto
 */
public class DemoProperties_answer extends Application implements Runnable {
    
    @Override
    public void run() {
        MyAppModel.USERS.sizeProperty().addListener(this::userSizeChanged);
        MyAppModel.createUser("user1");
        MyAppModel.createUser("user2");
        MyAppModel.createUser("user3");
        User user = MyAppModel.createUser("user4");
        user.nameProperty().addListener(this::userRenamed);
        user.setName("Luke");
        MyAppModel.USERS.remove(user);
    }

    private void userSizeChanged(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        int oldSize = oldValue.intValue();
        int newSize = newValue.intValue();
        if (newSize > oldSize) {
            System.out.println(String.format("%d user(s) added, new users count is %d", newSize - oldSize, newSize));
        } else if (newSize < oldSize) {
            System.out.println(String.format("%d user(s) removed, new users count is %d", oldSize - newSize, newSize));
        }
    }

    private void userRenamed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        System.out.println(String.format("renamed '%s' to '%s'", oldValue, newValue));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new DemoProperties_answer().run();
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    }

}
