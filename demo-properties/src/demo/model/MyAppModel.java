/*
 * MyAppModel.java
 */
package demo.model;

import javafx.beans.property.ReadOnlyListWrapper;
import javafx.collections.FXCollections;

/**
 *
 * @author Yann D'Isanto
 */
public interface MyAppModel {

    ReadOnlyListWrapper<User> USERS = new ReadOnlyListWrapper<>(FXCollections.observableArrayList());
    
    static User createUser(String name) {
        User user = new User(name);
        USERS.add(user);
        return user;
    }
}
