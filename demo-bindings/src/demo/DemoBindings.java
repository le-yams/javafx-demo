/*
 * Main1.java
 */
package demo;

import demo.model.MyAppModel;
import demo.model.Task;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 * TODO: 
 *   - use binding to dynamically compute task effective over estimated labour percent
 *   - writes a binding computing the tasks count for a given user (answer in demo.bindings package).
 * 
 * @author Yann D'Isanto
 */
public class DemoBindings extends Application implements Runnable {
    
    @Override
    public void run() {
        Task task = MyAppModel.createTask("task");
        System.out.println(computeTaskEffectiveOverEstimated(task) + "%");
        task.setEstimatedLabour(100);
        task.setEffectiveLabour(50);
        System.out.println(computeTaskEffectiveOverEstimated(task) + "%");
    }

    private double computeTaskEffectiveOverEstimated(Task task) {
        return (double) task.getEffectiveLabour() / (double) task.getEstimatedLabour() * 100.0;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new DemoBindings().run();
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    }

}
