/*
 * Main1.java
 */
package demo;

import demo.model.MyAppModel;
import demo.model.Task;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.value.ObservableValue;
import javafx.stage.Stage;

/**
 *
 * @author Yann D'Isanto
 */
public class DemoBindings_answer extends Application implements Runnable {

    @Override
    public void run() {
        Task task = MyAppModel.createTask("task");
        
        Bindings.divide(
            asDouble(task.effectiveLabourProperty()), 
            asDouble(task.estimatedLabourProperty())
        ).multiply(100.0)
            .addListener(this::labourRatioChanged);
//        asDouble(task.effectiveLabourProperty())
//            .divide( asDouble(task.estimatedLabourProperty()) )
//            .multiply(100.0)
//            .addListener(this::labourRatioChanged);

        task.setEstimatedLabour(100);
        task.setEffectiveLabour(50);

    }

    private void labourRatioChanged(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        System.out.println(String.format("labour ratio changed: %.2f%% -> %.2f%%", oldValue, newValue));
    }
    
    public static DoubleExpression asDouble(ObservableValue<Number> number) {
        return new NumberAsDoubleBinding(number);
    }

    private static class NumberAsDoubleBinding extends DoubleBinding {

        private final ObservableValue<Number> number;

        public NumberAsDoubleBinding(ObservableValue<Number> number) {
            bind(number);
            this.number = number;
        }

        @Override
        protected double computeValue() {
            return number.getValue().doubleValue();
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new DemoBindings_answer().run();
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    }

}
