package demo.bindings;

import demo.model.MyAppModel;
import demo.model.Task;
import demo.model.Task.State;
import demo.model.User;
import javafx.beans.Observable;

/**
 *
 * @author Yann D'Isanto
 */
public class UserTasksWithStateCount extends DeepListBinding<Task, Double> {

    private final User user;

    private final State state;

    public UserTasksWithStateCount(User user, State state) {
        super(MyAppModel.TASKS);
        this.user = user;
        this.state = state;
    }

    @Override
    protected Observable[] elementObservables(Task task) {
        return new Observable[]{task.ownerProperty(), task.stateProperty()};
    }

    @Override
    protected Double computeValue() {
        return (double) MyAppModel.TASKS.stream().filter(this::filterTask).count();
    }

    private boolean filterTask(Task task) {
        return user.equals(task.getOwner()) && task.getState() == state;
    }
}
