
package demo.bindings;

import demo.model.MyAppModel;
import demo.model.Task;
import demo.model.User;
import javafx.beans.Observable;

/**
 *
 * @author Yann D'Isanto
 */
public class UserTasksCount extends DeepListBinding<Task, Long> {

    private final User user;

    public UserTasksCount(User user) {
        super(MyAppModel.TASKS);
        this.user = user;
    }
    
    
    @Override
    protected Observable[] elementObservables(Task task) {
        return new Observable[] {task.ownerProperty()};
    }

    @Override
    protected Long computeValue() {
        return MyAppModel.TASKS.stream().filter(this::filterTask).count();
    }
    
    private boolean filterTask(Task task) {
        return user.equals(task.getOwner());
    }
}
