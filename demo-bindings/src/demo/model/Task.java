/*
 * Task.java
 */
package demo.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author Yann D'Isanto
 */
public class Task extends Nameable {

    private final ObjectProperty<User> owner;

    private final ObjectProperty<State> state;

    private final IntegerProperty estimatedLabour;

    private final IntegerProperty effectiveLabour;
    
    public Task(String name) {
        this(name, null, State.TODO, 0, 0);
    }
    
    
    public Task(String name, User owner, State state, int estimatedLabour, int effectiveLabour) {
        super(name);
        this.owner = new SimpleObjectProperty<>(owner);
        this.state = new SimpleObjectProperty<>(state);
        this.estimatedLabour = new SimpleIntegerProperty(estimatedLabour);
        this.effectiveLabour = new SimpleIntegerProperty(effectiveLabour);
    }

    public User getOwner() {
        return owner.get();
    }

    public void setOwner(User value) {
        owner.set(value);
    }

    public ObjectProperty<User> ownerProperty() {
        return owner;
    }

    public State getState() {
        return state.get();
    }

    public void setState(State value) {
        state.set(value);
    }

    public ObjectProperty<State> stateProperty() {
        return state;
    }

    public int getEstimatedLabour() {
        return estimatedLabour.get();
    }

    public void setEstimatedLabour(int value) {
        estimatedLabour.set(value);
    }

    public IntegerProperty estimatedLabourProperty() {
        return estimatedLabour;
    }

    public int getEffectiveLabour() {
        return effectiveLabour.get();
    }

    public void setEffectiveLabour(int value) {
        effectiveLabour.set(value);
    }

    public IntegerProperty effectiveLabourProperty() {
        return effectiveLabour;
    }

    public static enum State {

        TODO,
        WORK_IN_PROGRESS,
        DONE

    }
}
