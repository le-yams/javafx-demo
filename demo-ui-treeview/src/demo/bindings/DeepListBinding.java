/*
 * DeepListBinding.java
 */
package demo.bindings;

import javafx.beans.Observable;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;

/**
 *
 * @author Yann D'Isanto
 * @param <E> list elements type
 * @param <T> binding value type
 */
public abstract class DeepListBinding<E, T> extends ObjectBinding<T> {

    public DeepListBinding(ListProperty<E> list) {
        bind(list.sizeProperty());
        list.addListener(this::onListChanged);
        list.addListener(this::onListContentChanged);
    }

    private void onListChanged(ObservableValue<? extends ObservableList<E>> observable, ObservableList<E> oldValue, ObservableList<E> newValue) {
        if (oldValue != null) {
            oldValue.forEach(this::unbindElement);
        }
        if (newValue != null) {
            newValue.forEach(this::bindElement);
        }
    }

    private void onListContentChanged(Change<? extends E> c) {
        while (c.next()) {
            if (c.wasPermutated() == false) {
                c.getRemoved().forEach(this::unbindElement);
                c.getAddedSubList().forEach(this::bindElement);
            }
        }
    }

    private void bindElement(E element) {
        bind(elementObservables(element));
    }

    private void unbindElement(E element) {
        unbind(elementObservables(element));
    }

    protected abstract Observable[] elementObservables(E element);
}
