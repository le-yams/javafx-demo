/*
 * MainProperties.java
 */
package demo;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * TODO:
 *   - add a selectedUser property in DemoTreeViewController
 *   - add a selectedTask property in DemoTreeViewController
 *   - modify UserNode to dynamically display user name change
 *   - modify UserNode to display a grayed icon if user is disabled (icons are provided in demo.ui.resources package)
 *   - modify TaskNode to dynamically display the task name followed by the task owner name.
 * 
 * @author Yann D'Isanto
 */
public class DemoTreeView extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ui/DemoTreeView.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
