/*
 * User.java
 */
package demo.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 *
 * @author Yann D'Isanto
 */
public class User extends Nameable {

    private final BooleanProperty disabled = new SimpleBooleanProperty(false);

    public User(String name) {
        super(name);
    }

    public boolean isDisabled() {
        return disabled.get();
    }

    public void setDisabled(boolean value) {
        disabled.set(value);
    }

    public BooleanProperty disabledProperty() {
        return disabled;
    }
}
