/*
 * Nameable.java
 */
package demo.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Yann D'Isanto
 */
public class Nameable {
    
    private final StringProperty name;

    public Nameable(String name) {
        this.name = new SimpleStringProperty(name);
    }
    
    public String getName() {
        return name.get();
    }

    public void setName(String value) {
        name.set(value);
    }

    public StringProperty nameProperty() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
