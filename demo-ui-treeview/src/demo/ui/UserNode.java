package demo.ui;

import demo.model.User;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 * A User tree node.
 * 
 * @author Yann D'Isanto
 */
public class UserNode extends TreeItem<String> {

    private final User user;

    public UserNode(User user) {
        super(user.getName(), new ImageView("/demo/ui/resources/user.png"));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

}
