package demo.ui;

import demo.model.Task;
import javafx.beans.binding.StringBinding;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 *
 * @author Yann D'Isanto
 */
public class TaskNode_answer extends TreeItem<String> {

    private final Task task;

    public TaskNode_answer(Task task) {
        super(task.getName(), new ImageView("/demo/ui/resources/task.png"));
        this.task = task;
        valueProperty().bind(new StringBinding() {
            {
                bind(task.nameProperty(), task.ownerProperty());
            }

            @Override
            protected String computeValue() {
                StringBuilder sb = new StringBuilder().append(task.getName());
                if (task.getOwner() != null) {
                    sb.append(" - ").append(task.getOwner());
                }
                return sb.toString();
            }
        });
    }

    public Task getTask() {
        return task;
    }

}
