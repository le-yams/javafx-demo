/*
 * UserPane.java
 */
package demo.ui.components;

import demo.model.MyAppModel;
import demo.model.Task;
import demo.model.User;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.NavigableMap;
import java.util.ResourceBundle;
import java.util.TreeMap;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.converter.NumberStringConverter;

/**
 * FXML Controller class
 *
 * @author Yann D'Isanto
 */
public class TaskPane extends GridPane implements Initializable {

    private final ObjectProperty<Task> task = new SimpleObjectProperty<>();

    @FXML
    TextField nameField;

    @FXML
    ComboBox<User> ownerField;

    @FXML
    ComboBox<Task.State> stateField;

    @FXML
    PositiveIntegerField estimatedLabourField;

    @FXML
    PositiveIntegerField effectiveLabourField;

    @FXML
    Label labourRatioField;
    
    @FXML
    ProgressBar labourRatioProgressField;

    public TaskPane() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TaskPane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        disableProperty().bind(task.isNull());
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ownerField.setItems(MyAppModel.USERS);
        stateField.setItems(FXCollections.observableArrayList(Task.State.values()));
        task.addListener(this::taskChanged);
        labourRatioProgressField.styleProperty().bind(new StringBinding() {

            {
                bind(labourRatioProgressField.progressProperty());
            }
            
            @Override
            protected String computeValue() {
                Map.Entry<Double, String> entry = PROGRESS_STYLES.floorEntry(labourRatioProgressField.getProgress());
                return entry != null ? entry.getValue() : "";
            }
        });
    }
    
    private static final NavigableMap<Double, String> PROGRESS_STYLES = new TreeMap<>();
    static {
        PROGRESS_STYLES.put(0.0, "-fx-accent: red;");
        PROGRESS_STYLES.put(0.25, "-fx-accent: orange;");
        PROGRESS_STYLES.put(0.40, "-fx-accent: yellow;");
        PROGRESS_STYLES.put(0.60, "-fx-accent: blue;");
        PROGRESS_STYLES.put(1.0, "-fx-accent: green;");
        PROGRESS_STYLES.put(1.1, "-fx-accent: blue;");
        PROGRESS_STYLES.put(1.5, "-fx-accent: yellow;");
        PROGRESS_STYLES.put(1.8, "-fx-accent: orange;");
        PROGRESS_STYLES.put(2.0, "-fx-accent: red;");
        
    }

    private void taskChanged(ObservableValue<? extends Task> observable, Task oldTask, Task newTask) {
        if (oldTask != null) {
            nameField.textProperty().unbindBidirectional(oldTask.nameProperty());
            oldTask.ownerProperty().unbind();
            oldTask.stateProperty().unbind();
            estimatedLabourField.textProperty().unbindBidirectional(oldTask.estimatedLabourProperty());
            effectiveLabourField.textProperty().unbindBidirectional(oldTask.effectiveLabourProperty());
            labourRatioField.textProperty().unbind();
            labourRatioProgressField.progressProperty().unbind();
        }
        if (newTask != null) {
            nameField.textProperty().bindBidirectional(newTask.nameProperty());
            ownerField.getSelectionModel().select(newTask.getOwner());
            newTask.ownerProperty().bind(ownerField.getSelectionModel().selectedItemProperty());
            stateField.getSelectionModel().select(newTask.getState());
            newTask.stateProperty().bind(stateField.getSelectionModel().selectedItemProperty());
            estimatedLabourField.textProperty().bindBidirectional(
                    newTask.estimatedLabourProperty(), 
                    new NumberStringConverter("#0"));
            effectiveLabourField.textProperty().bindBidirectional(
                    newTask.effectiveLabourProperty(), 
                    new NumberStringConverter("#0"));
            labourRatioField.textProperty().bind(newTask.effectiveOverEstimatedProperty().multiply(100).asString("%.2f%%"));
            labourRatioProgressField.progressProperty().bind(newTask.effectiveOverEstimatedProperty());
        }
    }

    public Task getTask() {
        return task.get();
    }

    public void setTask(Task value) {
        task.set(value);
    }

    public ObjectProperty<Task> taskProperty() {
        return task;
    }

    public static void showEditDialog(Window owner, Task task) {
        TaskEditDialog dialog = new TaskEditDialog(owner, task);
        dialog.showAndWait();
    }

    public static Task showCreateDialog(Window owner) {
        TaskCreateDialog dialog = new TaskCreateDialog(owner);
        dialog.showAndWait();
        if (dialog.ret == TaskCreateDialog.OK) {
            return dialog.taskPane.getTask();
        }
        return null;
    }

    static abstract class AbstractTaskDialog extends Stage {

        static final int OK = 1;

        static final int CANCEL = 2;

        static final int CLOSE = 3;

        final TaskPane taskPane = new TaskPane();

        final HBox buttons = new HBox(5);

        int ret = CLOSE;

        public AbstractTaskDialog(Window owner) {
            initOwner(owner);
            initModality(Modality.APPLICATION_MODAL);
            buttons.setAlignment(Pos.CENTER_RIGHT);
            GridPane gridPane = new GridPane();
            gridPane.add(taskPane, 0, 0, 2, 1);
            gridPane.add(buttons, 1, 1);
            GridPane.setVgrow(buttons, Priority.NEVER);
            GridPane.setHgrow(buttons, Priority.NEVER);
            GridPane.setMargin(buttons, new Insets(20, 5, 5, 5));
            Scene scene = new Scene(gridPane);
            setScene(scene);
        }

        void closeAction(ActionEvent event) {
            this.close();
        }

        void okAction(ActionEvent event) {
            MyAppModel.TASKS.add(taskPane.getTask());
            ret = OK;
            this.close();
        }

        void cancelAction(ActionEvent event) {
            ret = CANCEL;
            this.close();
        }
    }

    static class TaskCreateDialog extends AbstractTaskDialog {

        private static int id = 1;
                
        public TaskCreateDialog(Window owner) {
            super(owner);
            Button okButton = new Button("Ok");
            Button cancelButton = new Button("Cancel");
            okButton.setPrefWidth(70);
            cancelButton.setPrefWidth(70);
            okButton.setOnAction(this::okAction);
            cancelButton.setOnAction(this::cancelAction);
            buttons.getChildren().addAll(okButton, cancelButton);
            taskPane.setTask(new Task("Task " + nextId()));
        }

        private static int nextId() {
            return id ++;
        }
    }

    static class TaskEditDialog extends AbstractTaskDialog {

        public TaskEditDialog(Window owner, Task task) {
            super(owner);
            Button closeButton = new Button("Ok");
            closeButton.setOnAction(this::closeAction);
            buttons.getChildren().add(closeButton);
            taskPane.setTask(task);
        }
    }
}
