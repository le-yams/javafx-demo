package demo.ui;

import demo.model.Task;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 * A Task tree node.
 *
 * @author Yann D'Isanto
 */
public class TaskNode extends TreeItem<String> {

    private final Task task;

    public TaskNode(Task task) {
        super(task.getName(), new ImageView("/demo/ui/resources/task.png"));
        this.task = task;
    }

    public Task getTask() {
        return task;
    }

}
