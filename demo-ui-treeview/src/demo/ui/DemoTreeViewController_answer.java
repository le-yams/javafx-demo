/*
 * DemoFXMLController.java
 */
package demo.ui;

import demo.model.MyAppModel;
import demo.model.Task;
import demo.model.User;
import demo.ui.components.TaskPane;
import demo.ui.components.UserPane;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener.Change;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Yann D'Isanto
 */
public class DemoTreeViewController_answer implements Initializable {

    @FXML
    TreeView<String> treeView;

    TreeItem<String> usersNode = new TreeItem<>("Users", new ImageView("/demo/ui/resources/users.png"));

    TreeItem<String> tasksNode = new TreeItem<>("Tasks", new ImageView("/demo/ui/resources/folder_tasks.png"));

    @FXML
    Button editUserButton;

    @FXML
    Button editTaskButton;

    final Map<User, UserNode> userNodes = new HashMap<>();

    final Map<Task, TaskNode> taskNodes = new HashMap<>();

    private final ObjectProperty<User> selectedUser = new SimpleObjectProperty<>();
    
    private final ObjectProperty<Task> selectedTask = new SimpleObjectProperty<>();

    public User getSelectedUser() {
        return selectedUser.get();
    }

    public void setSelectedUser(User value) {
        selectedUser.set(value);
    }

    public ObjectProperty selectedUserProperty() {
        return selectedUser;
    }

    public Task getSelectedTask() {
        return selectedTask.get();
    }

    public void setSelectedTask(Task value) {
        selectedTask.set(value);
    }

    public ObjectProperty selectedTaskProperty() {
        return selectedTask;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TreeItem<String> treeRoot = new TreeItem<>("root");

        treeRoot.getChildren().addAll(usersNode, tasksNode);
        treeView.setRoot(treeRoot);
        treeView.setCellFactory((TreeView<String> param) -> {
            return new CustomCell();
        });
        MyAppModel.USERS.addListener(this::onUsersListChanged);
        MyAppModel.TASKS.addListener(this::onTasksListChanged);

        treeView.getSelectionModel().selectedItemProperty().addListener(this::treeItemSelected);
        selectedUser.addListener((ObservableValue<? extends User> observable, User oldValue, User newValue) -> {
            Optional<UserNode> userNode = usersNode.getChildren().stream()
                    .map(item -> (UserNode) item)
                    .filter(n -> n.getUser().equals(newValue))
                    .findAny();
            if(userNode.isPresent()) {
                treeView.getSelectionModel().select(userNode.get());
            }
        });
        selectedTask.addListener((ObservableValue<? extends Task> observable, Task oldValue, Task newValue) -> {
            Optional<TaskNode> taskNode = tasksNode.getChildren().stream()
                    .map(item -> (TaskNode) item)
                    .filter(n -> n.getTask().equals(newValue))
                    .findAny();
            if(taskNode.isPresent()) {
                treeView.getSelectionModel().select(taskNode.get());
            }
        });

        editUserButton.disableProperty().bind(selectedUserProperty().isNull());
        editTaskButton.disableProperty().bind(selectedTaskProperty().isNull());
    }

    private void treeItemSelected(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue, TreeItem<String> newValue) {
        if (newValue instanceof UserNode) {
            selectedUser.set(((UserNode) newValue).getUser());
            selectedTask.set(null);
        } else if (newValue instanceof TaskNode) {
            selectedUser.set(null);
            selectedTask.set(((TaskNode) newValue).getTask());
        } else {
            selectedUser.set(null);
            selectedTask.set(null);
        }
    }

    private void onUsersListChanged(Change<? extends User> c) {
        while (c.next()) {
            if (c.wasPermutated() == false) {
                for (User user : c.getRemoved()) {
                    UserNode node = userNodes.remove(user);
                    usersNode.getChildren().remove(node);
                }
                for (User user : c.getAddedSubList()) {
                    UserNode node = new UserNode(user);
                    usersNode.getChildren().add(node);
                    userNodes.put(user, node);
                    treeView.getSelectionModel().select(node);
                }
            }
        }
    }

    private void onTasksListChanged(Change<? extends Task> c) {

        while (c.next()) {
            if (c.wasPermutated() == false) {
                for (Task task : c.getRemoved()) {
                    TaskNode node = taskNodes.remove(task);
                    tasksNode.getChildren().remove(node);
                }
                for (Task task : c.getAddedSubList()) {
                    TaskNode node = new TaskNode(task);
                    tasksNode.getChildren().add(node);
                    taskNodes.put(task, node);
                    treeView.getSelectionModel().select(node);
                }
            }
        }
    }

    @FXML
    void close(ActionEvent evt) {
        ((Stage) getWindow()).close();
    }

    @FXML
    void addUser(ActionEvent evt) {
        UserPane.showCreateDialog(getWindow());
        usersNode.setExpanded(true);
    }

    @FXML
    void editSelectedUser(ActionEvent evt) {
        UserPane.showEditDialog(getWindow(), getSelectedUser());
    }

    @FXML
    void addTask(ActionEvent evt) {
        TaskPane.showCreateDialog(getWindow());
        tasksNode.setExpanded(true);
    }

    @FXML
    void editSelectedTask(ActionEvent evt) {
        TaskPane.showEditDialog(getWindow(), getSelectedTask());
    }

    private Window getWindow() {
        return treeView.getScene().getWindow();
    }

    /**
     * Custom tree cell with context menu.
     */
    class CustomCell extends TextFieldTreeCell<String> {

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            TreeItem<String> node = getTreeItem();
            if (node instanceof UserNode) {
                setContextMenu(createContextMenu((ActionEvent t) -> {
                            UserPane.showEditDialog(getWindow(), ((UserNode) node).getUser());
                        }, (ActionEvent t) -> {
                            MyAppModel.USERS.remove(((UserNode) node).getUser());
                        }));
            } else if (node instanceof TaskNode) {
                setContextMenu(createContextMenu((ActionEvent t) -> {
                            TaskPane.showEditDialog(getWindow(), ((TaskNode) node).getTask());
                        }, (ActionEvent t) -> {
                            MyAppModel.TASKS.remove(((TaskNode) node).getTask());
                        }));
            } else {
                setContextMenu(null);
            }
        }

        private ContextMenu createContextMenu(EventHandler<ActionEvent> editAction, EventHandler<ActionEvent> removeAction) {
            ContextMenu menu = new ContextMenu();
            MenuItem editMenuItem = new MenuItem("Edit");
            editMenuItem.setOnAction(editAction);
            MenuItem removeMenuItem = new MenuItem("Remove");
            removeMenuItem.setOnAction(removeAction);
            menu.getItems().addAll(editMenuItem, new SeparatorMenuItem(), removeMenuItem);
            return menu;
        }

    }

}
