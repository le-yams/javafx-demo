package demo.ui;

import demo.model.User;
import javafx.beans.binding.ObjectBinding;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

/**
 *
 * @author Yann D'Isanto
 */
public class UserNode_answer extends TreeItem<String> {

    private final ImageView icon = new ImageView("/demo/ui/resources/user.png");
    private final ImageView disabledIcon = new ImageView("/demo/ui/resources/user_grayed.png");
    private final User user;

    public UserNode_answer(User user) {
        super();
        this.user = user;
        valueProperty().bind(user.nameProperty());
        graphicProperty().bind(new ObjectBinding<ImageView>() {
            {
                bind(user.disabledProperty());
            }

            @Override
            protected ImageView computeValue() {
                return user.isDisabled() ? disabledIcon : icon;
            }
        });
    }

    public User getUser() {
        return user;
    }

}
